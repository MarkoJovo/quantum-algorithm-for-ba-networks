# Marko Jovanovic marko285@gmail.com
# Ran in python 3.8

import numpy as np
import math
from qiskit import QuantumCircuit, execute, QuantumRegister,ClassicalRegister, Aer
from qiskit.quantum_info import random_statevector
from qiskit.extensions import Initialize
from qiskit.quantum_info.operators import Operator
#from qiskit.visualization import plot_histogram
import matplotlib.pyplot as plt

#Setting up simulator
sim = Aer.get_backend('qasm_simulator')


# Testing Parameters. Change as desired
N = 27
B = 0.88
beta = math.sqrt(B)
#########

quantums = QuantumRegister(N)
classicals = ClassicalRegister(N)
circuit = QuantumCircuit(quantums,classicals)
 
print("N: ", N,"   B: ",B,"   Beta: ", beta)
for n in range(1,N+1):
    #print("now working on ", n)
    a = math.sqrt(1 - beta**(2*n) )
    c = beta**n
    b = -1*c
    
    #print("beta to the power of n, when n is ", n, " is ", c)

    op_n = Operator([
        [a,b],
        [c,a]
        ])

    circuit.unitary(op_n, [n-1], label = "F_"+str(n))
    circuit.measure(n-1,n-1)
    

#print("done with everything")
print(circuit)


#Run N times and print results
res = execute(circuit, sim, shots=N).result()
counts = res.get_counts(circuit)
print("Counts are: ",counts)

countsfile = open("BitStringResultCounts.txt", "w")
for i in counts:
    countsfile.write(i)
    countsfile.write(" ")
    countsfile.write(str(counts[i]))
    countsfile.write("\n")
countsfile.close()

#print("next")


outputMatrix = list()

for i in counts:
    for x in range(counts[i]):
        entry = list(i)
        entry.reverse()
        outputMatrix.append( entry )
#print("old matrix")
#print(outputMatrix)

#print("transposed matrix")
outputMatrix = np.transpose(outputMatrix)
#print(outputMatrix)

matlabfile = open("AdjMatrixMatLabForm.txt", 'w')
matlabfile.write("[")
for line in outputMatrix:
    outline = " ".join(line)
    matlabfile.write(outline)
    print(outline, ";")
#    matlabfile.write(";\n")
    matlabfile.write("; ")

matlabfile.write("];")
matlabfile.close()




########## Position Counts (Can be run more than N times, to verify probability distribution)
my_bits = {}

for i in counts:
#    print(counts[i])
    positions = list(i)
    positions = list(map(int,positions))
    total = sum(positions)
    positions = list(map(str, positions))
    positions.reverse()
    if total == 0:
       print("item "+str(i)+" has no 1 bits")
    else:
        for x in range(total):
            numb = positions.index("1")
            if numb in my_bits:
                #my_bits[numb] += 1
                my_bits[numb] += counts[i]

                #print("adding to ", numb, " for ", i)
                positions[numb] = "0"
            else:
                #my_bits[numb] = 1
                my_bits[numb] = counts[i]
        #        print("starting position ", numb, " count for ", i)
                positions[numb] = "0"

#print(my_bits)
#print("the end")

#plt.bar(list(my_bits.keys()),list(my_bits.values()))
#plt.show()


